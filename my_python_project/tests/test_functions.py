from my_python_project import coalesce


def test_coalesce():
    assert coalesce(None, 1) == 1
    assert coalesce(1, None) == 1
    assert coalesce(None, None) is None
